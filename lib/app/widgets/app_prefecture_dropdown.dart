import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';
import 'package:plat_app/base/resources/constants/radius.dart';
import 'package:plat_app/base/resources/constants/base_text_styles.dart';
import 'package:plat_app/app/widgets/app_dropdown.dart';

class AppPrefectureDropdown extends StatelessWidget {
  const AppPrefectureDropdown(
      {Key? key,
      required this.onChanged,
      required this.validationMessage,
      required this.currentValue,
      required this.values})
      : super(key: key);
  final AppPrefectureDropdownValue? currentValue;
  final String? validationMessage;
  final Function(AppPrefectureDropdownValue?) onChanged;
  final List<AppPrefectureDropdownValue> values;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: dimen12),
      child: Column(
        children: [
          Row(
            children: [
              RichText(
                text: TextSpan(
                  text: 'prefecture'.tr,
                  style: text14_2C2C2C_600,
                  children: [
                    TextSpan(
                        text: ' (*)',
                        style: text14_2C2C2C_600.copyWith(color: colorError)),
                  ],
                ),
              ),
            ],
          ),
          verticalSpace4,
          Container(
            padding: EdgeInsets.only(left: dimen16, right: dimen10),
            width: double.infinity,
            decoration: BoxDecoration(
                color: colorF5F7F9,
                borderRadius: border8,
                border: (validationMessage?.isNotEmpty == true)
                    ? Border.all(color: colorError, width: dimen1)
                    : null),
            child: DropdownButtonHideUnderline(
              child: AppDropdownButton<
                  AppPrefectureDropdownValue>(
                value: currentValue,
                hint: Text(
                  'please_select_option'.tr,
                  style: text14_898989_400,
                ),
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: color898989,
                ),
                style: text16_2C2C2C_600,
                onChanged:
                    (AppPrefectureDropdownValue? newValue) {
                  onChanged(newValue);
                },
                items: values.map<
                        DropdownMenuItem<
                            AppPrefectureDropdownValue>>(
                    (AppPrefectureDropdownValue value) {
                  return DropdownMenuItem<
                      AppPrefectureDropdownValue>(
                    value: value,
                    child: Text(
                      value.value,
                      style: text14_2C2C2C_400,
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          (validationMessage?.isNotEmpty == true)
              ? verticalSpace4
              : Container(),
          (validationMessage?.isNotEmpty == true)
              ? Row(
                  children: [
                    Text(
                      '$validationMessage',
                      style: text12_error_400,
                    ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }
}

class AppPrefectureDropdownValue {
  /// JP01 愛知県
  /// JP02 愛知県
  AppPrefectureDropdownValue(
      {required this.key, required this.value});

  final String key;
  final String value;
}
