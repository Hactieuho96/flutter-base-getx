import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:plat_app/app/widgets/app_button.dart';
import 'package:plat_app/app/widgets/app_input_widget.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';
import 'package:plat_app/base/resources/constants/base_text_styles.dart';

class AppPostalCode extends StatelessWidget {
  const AppPostalCode(
      {Key? key,
      required this.zipCodeController,
      this.zipCodeValidationMessage,
      this.hint,
      this.onSubmitted,
      this.textInputAction,
      required this.validate})
      : super(key: key);
  final TextEditingController zipCodeController;
  final String? zipCodeValidationMessage;
  final String? hint;
  final ValueChanged<String>? onSubmitted;
  final TextInputAction? textInputAction;
  final Function(bool) validate;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(
        children: [
          Text(
            'postal_code_zip'.tr,
            style: text14_2C2C2C_600,
          ),
          Text(
            ' (*)',
            style: text14_error_600,
          )
        ],
      ),
      Container(
        margin: EdgeInsets.only(top: dimen2, bottom: dimen12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
                child: AppInputView(
              controller: zipCodeController,
              textInputAction: TextInputAction.next,
              hint: hint ?? 'postal_code_hint'.tr,
              maxLength: 7,
              keyboardType: TextInputType.number,
              validationMessage: zipCodeValidationMessage,
            )),
            horizontalSpace8,
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: dimen3),
                child: AppButton(
                  title: 'search'.tr,
                  onTap: () {
                    // Validate
                    validate(true);
                  },
                  height: dimen40,
                ),
              ),
            ),
          ],
        ),
      )
    ]);
  }
}
