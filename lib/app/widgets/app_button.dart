import 'package:flutter/material.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/base_text_styles.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';

class AppButton extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final double margin;
  final double horizontalPadding;
  final VoidCallback? onTap;
  final Color textColor;
  final bool isEnable;
  final bool isPrimaryStyle;
  final Color disableBackgroundColor;
  final Color disableTextColor;
  final double height;
  final TextStyle? textStyle;
  static const DEFAULT_BUTTON_HEIGHT = dimen48;

  const AppButton(
      {Key? key,
      this.title = '',
      this.backgroundColor = colorPrimary,
      this.margin = dimen0,
      this.horizontalPadding = dimen32,
      this.onTap,
      this.textColor = colorWhite,
      this.isEnable = true,
      this.disableBackgroundColor = colorDisabledButton,
      this.disableTextColor = colorWhite,
      this.isPrimaryStyle = true,
      this.height = DEFAULT_BUTTON_HEIGHT,
      this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: isEnable ? onTap : null,
      borderRadius: const BorderRadius.all(
          Radius.circular(DEFAULT_BUTTON_HEIGHT / 2)),
      child: Container(
        height: height,
        margin: EdgeInsets.all(margin),
        padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
              Radius.circular(DEFAULT_BUTTON_HEIGHT / 2)),
          color: !isEnable
              ? disableBackgroundColor
              : isPrimaryStyle
                  ? backgroundColor
                  : colorF5F7F9,
        ),
        child: Center(
          child: Text(title,
              style: textStyle ??
                  (isPrimaryStyle ? text14_white_600 : text14_primary_600),
              maxLines: 1,
              overflow: TextOverflow.ellipsis),
        ),
      ),
    );
  }
}
