import 'package:plat_app/base/provider/base/base_provider.dart';
import 'package:get/get.dart';

class NotificationProvider extends BaseProvider {
  // Get all notifications
  Future<Response<dynamic>> fetchAllNotifications(page, limit) =>
      get('/notification?page=$page&limit=$limit');

  // Get notification detail
  Future<Response<dynamic>> fetchNotificationDetail(String id) {
    return get('/notification?id=$id');
  }
}
