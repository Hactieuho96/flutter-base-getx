class NotificationResponse {
  NotificationResponse({
      Glossary? glossary,}){
    _glossary = glossary;
}

  NotificationResponse.fromJson(dynamic json) {
    _glossary = json['glossary'] != null ? Glossary.fromJson(json['glossary']) : null;
  }
  Glossary? _glossary;
NotificationResponse copyWith({  Glossary? glossary,
}) => NotificationResponse(  glossary: glossary ?? _glossary,
);
  Glossary? get glossary => _glossary;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_glossary != null) {
      map['glossary'] = _glossary?.toJson();
    }
    return map;
  }

}

class Glossary {
  Glossary({
      String? title, 
      GlossDiv? glossDiv,}){
    _title = title;
    _glossDiv = glossDiv;
}

  Glossary.fromJson(dynamic json) {
    _title = json['title'];
    _glossDiv = json['GlossDiv'] != null ? GlossDiv.fromJson(json['GlossDiv']) : null;
  }
  String? _title;
  GlossDiv? _glossDiv;
Glossary copyWith({  String? title,
  GlossDiv? glossDiv,
}) => Glossary(  title: title ?? _title,
  glossDiv: glossDiv ?? _glossDiv,
);
  String? get title => _title;
  GlossDiv? get glossDiv => _glossDiv;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = _title;
    if (_glossDiv != null) {
      map['GlossDiv'] = _glossDiv?.toJson();
    }
    return map;
  }

}

class GlossDiv {
  GlossDiv({
      String? title, 
      GlossList? glossList,}){
    _title = title;
    _glossList = glossList;
}

  GlossDiv.fromJson(dynamic json) {
    _title = json['title'];
    _glossList = json['GlossList'];
  }
  String? _title;
  GlossList? _glossList;
GlossDiv copyWith({  String? title,
  GlossList? glossList,
}) => GlossDiv(  title: title ?? _title,
  glossList: glossList ?? _glossList,
);
  String? get title => _title;
  GlossList? get glossList => _glossList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = _title;
    map['GlossList'] = _glossList;
    return map;
  }

}

class GlossList {
  GlossList({
      GlossEntry? glossEntry,}){
    _glossEntry = glossEntry;
}

  GlossList.fromJson(dynamic json) {
    _glossEntry = json['GlossEntry'] != null ? GlossEntry.fromJson(json['GlossEntry']) : null;
  }
  GlossEntry? _glossEntry;
GlossList copyWith({  GlossEntry? glossEntry,
}) => GlossList(  glossEntry: glossEntry ?? _glossEntry,
);
  GlossEntry? get glossEntry => _glossEntry;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_glossEntry != null) {
      map['GlossEntry'] = _glossEntry?.toJson();
    }
    return map;
  }

}

class GlossEntry {
  GlossEntry({
      String? id, 
      String? sortAs, 
      String? glossTerm, 
      String? acronym, 
      String? abbrev, 
      GlossDef? glossDef, 
      String? glossSee,}){
    _id = id;
    _sortAs = sortAs;
    _glossTerm = glossTerm;
    _acronym = acronym;
    _abbrev = abbrev;
    _glossDef = glossDef;
    _glossSee = glossSee;
}

  GlossEntry.fromJson(dynamic json) {
    _id = json['ID'];
    _sortAs = json['SortAs'];
    _glossTerm = json['GlossTerm'];
    _acronym = json['Acronym'];
    _abbrev = json['Abbrev'];
    _glossDef = json['GlossDef'] != null ? GlossDef.fromJson(json['GlossDef']) : null;
    _glossSee = json['GlossSee'];
  }
  String? _id;
  String? _sortAs;
  String? _glossTerm;
  String? _acronym;
  String? _abbrev;
  GlossDef? _glossDef;
  String? _glossSee;
GlossEntry copyWith({  String? id,
  String? sortAs,
  String? glossTerm,
  String? acronym,
  String? abbrev,
  GlossDef? glossDef,
  String? glossSee,
}) => GlossEntry(  id: id ?? _id,
  sortAs: sortAs ?? _sortAs,
  glossTerm: glossTerm ?? _glossTerm,
  acronym: acronym ?? _acronym,
  abbrev: abbrev ?? _abbrev,
  glossDef: glossDef ?? _glossDef,
  glossSee: glossSee ?? _glossSee,
);
  String? get id => _id;
  String? get sortAs => _sortAs;
  String? get glossTerm => _glossTerm;
  String? get acronym => _acronym;
  String? get abbrev => _abbrev;
  GlossDef? get glossDef => _glossDef;
  String? get glossSee => _glossSee;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['ID'] = _id;
    map['SortAs'] = _sortAs;
    map['GlossTerm'] = _glossTerm;
    map['Acronym'] = _acronym;
    map['Abbrev'] = _abbrev;
    if (_glossDef != null) {
      map['GlossDef'] = _glossDef?.toJson();
    }
    map['GlossSee'] = _glossSee;
    return map;
  }

}

class GlossDef {
  GlossDef({
      String? para, 
      List<String>? glossSeeAlso,}){
    _para = para;
    _glossSeeAlso = glossSeeAlso;
}

  GlossDef.fromJson(dynamic json) {
    _para = json['para'];
    _glossSeeAlso = json['GlossSeeAlso'] != null ? json['GlossSeeAlso'].cast<String>() : [];
  }
  String? _para;
  List<String>? _glossSeeAlso;
GlossDef copyWith({  String? para,
  List<String>? glossSeeAlso,
}) => GlossDef(  para: para ?? _para,
  glossSeeAlso: glossSeeAlso ?? _glossSeeAlso,
);
  String? get para => _para;
  List<String>? get glossSeeAlso => _glossSeeAlso;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['para'] = _para;
    map['GlossSeeAlso'] = _glossSeeAlso;
    return map;
  }

}