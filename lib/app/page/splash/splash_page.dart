import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:plat_app/base/routes/base_pages.dart';
import 'package:get/get.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    _goToLogin();
  }

  void _goToLogin() async {
    await Future.delayed(Duration(seconds: 1));
    Get.offAndToNamed(Routes.login);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Scaffold(body: SafeArea(child: Text('Splash'))));
  }
}
