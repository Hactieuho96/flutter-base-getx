import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';

class FullScreenProgress extends StatelessWidget {
  const FullScreenProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorDialogBackground,
      child: const Center(
        child: CircularProgressIndicator(color: colorPrimary,),
      ),
    );
  }
}
