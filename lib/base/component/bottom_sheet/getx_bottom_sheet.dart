import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetXBottomSheet {
  static void demoDefaultBottomSheet(content) {
    Get.bottomSheet(
      Container(
          height: dimen200,
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(dimen20),
                  topRight: Radius.circular(dimen20)),
              color: colorWhite),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ...content,
            ],
          )),
      isDismissible: true,
      enableDrag: true,
    );
  }
}
