import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';
import 'package:plat_app/app/widgets/app_button.dart';

class GetXDefaultDialog {
  static Future notifyDialog({title, middleText}) {
    if (Get.isDialogOpen == true) {
      Get.back();
    }
    if (title == null) {
      return Get.defaultDialog(
          title: '',
          middleText: middleText,
          titlePadding:
              EdgeInsets.only(top: dimen12, left: dimen16, right: dimen16),
          contentPadding: EdgeInsets.only(
              top: dimen8, bottom: dimen12, left: dimen16, right: dimen16),
          radius: dimen8);
    } else {
      return Get.defaultDialog(
        title: title,
        middleText: middleText,
        radius: dimen8,
        titlePadding:
            EdgeInsets.only(top: dimen12, left: dimen16, right: dimen16),
        contentPadding: EdgeInsets.only(
            top: dimen8, bottom: dimen4, left: dimen16, right: dimen16),
      );
    }
  }

  static Future alertDialog({title, middleText, textConfirm, onConfirm}) {
    if (Get.isDialogOpen == true) {
      Get.back();
    }
    return Get.defaultDialog(
        title: title,
        middleText: middleText,
        titlePadding: EdgeInsets.all(dimen16),
        contentPadding:
          EdgeInsets.only(left: dimen16, right: dimen16, bottom: dimen16),
        confirm: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppButton(
              title: textConfirm,
              horizontalPadding: dimen20,
              onTap: () {
                Get.back();
                (onConfirm ?? () {})();
              },
            ),
          ],
        ),
        confirmTextColor: colorWhite,
        radius: dimen8);
  }

  static Future successDialog({title, middleText, textConfirm, onConfirm, textStyle}) {
    if (Get.isDialogOpen == true) {
      Get.back();
    }
    return Get.defaultDialog(
        title: title,
        titleStyle: textStyle,
        middleText: middleText,
        titlePadding: const EdgeInsets.all(dimen16),
        contentPadding:
        const EdgeInsets.only(left: dimen16, right: dimen16, bottom: dimen16),
        confirm: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppButton(
              title: textConfirm,
              horizontalPadding: dimen20,
              onTap: () {
                Get.back();
                (onConfirm ?? () {})();
              },
            ),
          ],
        ),
        confirmTextColor: colorWhite,
        radius: dimen8);
  }

  static defaultDialog(
      {title, middleText, textConfirm, onConfirm, textCancel, onCancel}) async {
    if (Get.isDialogOpen == true) {
      Get.back();
    }
    return await Get.defaultDialog(
        title: title,
        middleText: middleText,
        textConfirm: textConfirm,
        confirmTextColor: colorWhite,
        onConfirm: () {
          Get.back();
          (onConfirm ?? () {})();
        },
        textCancel: textCancel,
        onCancel: () {
          (onCancel ?? () {})();
        },
        radius: dimen8);
  }

  static void demoDefaultDialog() {
    final language = Rx<String>('english');
    if (Get.isDialogOpen == true) {
      Get.back();
    }
    Get.defaultDialog(
      title: 'Choose a language',
      content: Column(
        children: <Widget>[
          RadioListTile(
            title: const Text('Tiếng Việt'),
            value: 'vietnamese',
            groupValue: language.value,
            onChanged: (String? value) {
              if (value != null) {
                Get.back();
                language.value = value;
              }
            },
          ),
          RadioListTile<String>(
            title: const Text('English'),
            value: 'english',
            groupValue: language.value,
            onChanged: (String? value) {
              if (value != null) {
                Get.back();
                language.value = value;
              }
            },
          ),
          RadioListTile<String>(
            title: const Text('Japanese'),
            value: 'japanese',
            groupValue: language.value,
            onChanged: (String? value) {
              if (value != null) {
                Get.back();
                language.value = value;
              }
            },
          ),
        ],
      ),
      radius: dimen8,
    );
  }
}
