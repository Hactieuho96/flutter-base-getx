import 'package:get/get.dart';
import 'package:plat_app/base/controllers/auth/auth_controller.dart';
import 'package:plat_app/base/provider/auth/auth_provider.dart';

class BaseLoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthProvider());
    Get.lazyPut(() => AuthController());
  }
}
