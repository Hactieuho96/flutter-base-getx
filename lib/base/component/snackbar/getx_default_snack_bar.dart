import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:plat_app/base/resources/constants/base_constraint.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';

class GetXDefaultSnackBar {
  static successSnackBar(
      {title = '',
      message = '',
      duration = const Duration(seconds: 3),
      snackPosition: SnackPosition.BOTTOM}) {
    return Get.rawSnackbar(icon:Image(
      image: AssetImage(getAssetImage(AssetImagePath.icon_snack_bar_success)),
      width: dimen28,
      height: dimen28,
    ), message: message,
        duration: duration, snackPosition: snackPosition, backgroundColor: colorDialogBackground);
  }

  static errorSnackBar(
      {title = '',
        message = '',
        duration = const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM}) {
    return Get.rawSnackbar(icon:Image(
      image: AssetImage(getAssetImage(AssetImagePath.icon_snack_bar_error)),
      width: dimen28,
      height: dimen28,
    ), message: message,
        duration: duration, snackPosition: snackPosition, backgroundColor: colorDialogBackground);
  }
}