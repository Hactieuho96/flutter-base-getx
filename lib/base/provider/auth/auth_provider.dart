import 'package:get/get.dart';
import 'package:plat_app/base/provider/base/base_provider.dart';

class AuthProvider extends BaseProvider {
  // Login request
  Future<Response<dynamic>> postLogin(body) => post('/login', body);

  // Logout request
  Future<Response<dynamic>> postLogout() {
    return post('/logout', null);
  }
}
