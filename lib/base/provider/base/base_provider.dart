import 'dart:developer';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:plat_app/base/controllers/auth/auth_controller.dart';
import 'package:plat_app/base/controllers/language/language_controller.dart';

class BaseProvider extends GetConnect {
  late AuthController authController = Get.find();
  final LanguageController languageController = Get.find();

  @override
  void onInit() {
    httpClient.baseUrl = dotenv.env['BASE_URL'];
    httpClient.timeout = const Duration(seconds: 30);
    httpClient.addRequestModifier<dynamic>((request) async {
      // Set accept
      request.headers['Accept'] = "application/json";
      // Set bearer token
      final token = authController.authData.value.data?.data?.accessToken;
      request.headers['Authorization'] = "Bearer $token";
      request.headers['X-localization'] =
          languageController.language.value.name;
      log('Added header: Bearer $token');
      log('Request:\nUrl: ${request.method.toUpperCase()} ${request.url}\nHeader: ${request.headers}');
      return request;
    });
    httpClient.addResponseModifier((request, response) async {
      log('Response: ${response.statusCode}${response.bodyString}');
      // TODO: Logout
      if (response.statusCode == 403) {
        authController.logout();
      }
      return response;
    });
  }
}
