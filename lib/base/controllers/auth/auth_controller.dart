import 'dart:convert';

import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';
import 'package:plat_app/base/component/page/login/controller/base_login_datasource.dart';
import 'package:plat_app/base/resources/constants/base_constraint.dart';
import 'package:plat_app/base/model/login_response.dart';
import 'package:plat_app/base/provider/auth/auth_provider.dart';
import 'package:plat_app/base/resources/network/network_resource.dart';

class AuthController extends GetxController {
  final storage = GetStorage();
  final authData = Rx(NetworkResource<LoginResponse>.init());
  late AuthProvider authProvider = Get.find();

  void login(email, password, remember) async {
    authData.value = NetworkResource<LoginResponse>.loading();
    final body = {'email': email, 'password': password};
    // final result = await authProvider.postLogin(body);
    // TODO: Mock login
    await Future.delayed(Duration(seconds: 1));
    final result = Response(
        body: mockBaseLoginResponseSuccess,
        bodyString: jsonEncode(mockBaseLoginResponseSuccess),
        statusCode: 200);
    NetworkResource.handleLoginResponse(result, onSuccess: (response) {
      _setLoggedIn(isLoggedIn: true, data: response);
      // Save email and password
      storage.write(keyRemember, remember);
      if (remember) {
        storage.write(keyEmail, email);
        storage.write(keyPassword, password);
      } else {
        storage.remove(keyEmail);
        storage.remove(keyPassword);
      }
    }, onFail: (errorMessage) {
      _setLoggedIn(isLoginError: true, message: errorMessage);
    });
  }

  bool isLoggingIn() {
    return authData.value.isLoading();
  }

  bool isLogInError() {
    return authData.value.isError();
  }

  bool isLoggedIn() {
    return authData.value.isSuccess();
  }

  void logout() async {
    authData.value =
        NetworkResource<LoginResponse>.loading(data: authData.value.data);
    // final result = await authProvider.postLogout();
    // TODO: Mock logout
    await Future.delayed(Duration(seconds: 1));
    final result = Response(
        body: mockBaseLogoutResponseSuccess,
        bodyString: jsonEncode(mockBaseLogoutResponseSuccess),
        statusCode: 200);
    if (result.isSuccess) {
      _setLoggedIn(isLoggedIn: false);
    } else {
      _setLoggedIn(isLoginError: true, message: result.statusText);
    }
  }

  bool isNotLoggedIn() {
    return authData.value.isInit();
  }

  void _setLoggedIn(
      {isLoggedIn = false,
      isLoginError = false,
      message,
      LoginResponse? data}) {
    if (isLoggedIn) {
      authData.value = NetworkResource<LoginResponse>.success(data: data);
    } else if (isLoginError) {
      authData.value = NetworkResource<LoginResponse>.error(message: message);
    } else {
      authData.value = NetworkResource<LoginResponse>.init();
    }
  }
}
