part of 'base_pages.dart';

abstract class Routes {
  static const splash = _Paths.splash;
  static const login = _Paths.login;
  static const forgotPassword = _Paths.login + _Paths.forgotPassword;
  static const alertForgotPassword = _Paths.login +  _Paths.forgotPassword + _Paths.alertForgotPassword;
  static const resetPasswordDeepLink = _Paths.resetPasswordDeepLink;
  static const home = _Paths.home;
  static const userSetting = _Paths.home + _Paths.userSetting;
  static const userInformation =
      _Paths.home + _Paths.userSetting + _Paths.userInformation;
  static const onlinePharmacy = _Paths.home + _Paths.onlinePharmacy;
  static const questionnaire =
      _Paths.home + _Paths.onlinePharmacy + _Paths.questionnaire;
  static const pharmacyList =
      _Paths.home + _Paths.onlinePharmacy + _Paths.pharmacyList;
  static const pharmacyInformation = _Paths.home +
      _Paths.onlinePharmacy +
      _Paths.pharmacyList +
      _Paths.pharmacyInformation;
  static const drugCategory = _Paths.home +
      _Paths.onlinePharmacy +
      _Paths.drugList +
      _Paths.drugCategory;
  static const drugList = _Paths.home + _Paths.onlinePharmacy + _Paths.drugList;
  static const drugDetail =
      _Paths.home + _Paths.onlinePharmacy + _Paths.drugDetail;
  static const orderHistory =
      _Paths.home + _Paths.onlinePharmacy + _Paths.orderHistory;
  static const orderHistoryDetail = _Paths.home +
      _Paths.onlinePharmacy +
      _Paths.orderHistory +
      _Paths.orderHistoryDetail;
  static const orderList =
      _Paths.home + _Paths.onlinePharmacy + _Paths.orderList;
  static const confirmOrder = _Paths.home +
      _Paths.onlinePharmacy +
      _Paths.orderList +
      _Paths.confirmOrder;
  static const registrationInformation =
      _Paths.home + _Paths.onlinePharmacy + _Paths.registrationInformation;
  static const consultationService = _Paths.home + _Paths.consultationService;
  static const completeRegistration = _Paths.completeRegistration;
  static const qa = _Paths.qa;
}

abstract class _Paths {
  static const splash = '/';
  static const login = '/login';
  static const loginDeepLink = '/:langCode/login';
  static const forgotPassword = '/forgotPassword';
  static const alertForgotPassword = '/alertForgotPassword';
  static const resetPasswordDeepLink = '/:langCode/reset-password';
  static const home = '/home';
  static const userSetting = '/userSetting';
  static const userInformation = '/userInformation';
  static const onlinePharmacy = '/onlinePharmacy';
  static const questionnaire = '/questionnaire';
  static const pharmacyList = '/pharmacyList';
  static const pharmacyInformation = '/pharmacyInformation';
  static const drugList = '/drugList';
  static const drugCategory = '/drugCategory';
  static const drugDetail = '/drugDetail';
  static const orderHistory = '/orderHistory';
  static const orderHistoryDetail = '/orderHistoryDetail';
  static const orderList = '/orderList';
  static const confirmOrder = '/confirmOrder';
  static const registrationInformation = '/registrationInformation';
  static const consultationService = '/consultationService';
  static const completeRegistration = '/completeRegistration';
  static const qa = '/qa';
}

void gotoProductDetailPage(productId) {
  Get.toNamed(Routes.drugDetail, arguments: {'product_id': productId});
}

void gotoOrderListPage() {
  Get.toNamed(Routes.orderList);
}
