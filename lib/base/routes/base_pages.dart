import 'package:get/get.dart';
import 'package:plat_app/base/component/page/home/binding/base_home_binding.dart';
import 'package:plat_app/base/component/page/home/page/base_home_page.dart';
import 'package:plat_app/base/component/page/login/page/base_login_page.dart';
import 'package:plat_app/base/component/page/login/binding/base_login_binding.dart';
import 'package:plat_app/base/component/page/splash/base_splash_page.dart';
import 'package:plat_app/base/routes/base_middleware.dart';

part 'base_routes.dart';

final List<GetPage<dynamic>> pages = [
  // TODO: Routing
  GetPage(
    name: _Paths.splash,
    page: () => const BaseSplashPage(),
  ),
  GetPage(
    name: _Paths.login,
    page: () => const BaseLoginPage(),
    binding: BaseLoginBinding(),
    middlewares: [
      BaseMiddleWare(),
    ]
  ),
  GetPage(
      name: _Paths.home,
      page: () => const BaseHomePage(),
      binding: BaseHomeBinding(),
      middlewares: [
        BaseMiddleWare(),
      ]
  ),
  // GetPage(
  //     name: _Paths.login,
  //     page: () => const LoginPage(),
  //     binding: AuthBinding(),
  //     middlewares: [
  //       AppMiddleWare(),
  //     ],
  //     children: [
  //       GetPage(
  //           name: _Paths.forgotPassword,
  //           page: () => const ForgotPasswordPage(),
  //           binding: ForgotPasswordBinding(),
  //           transition: Transition.rightToLeft,
  //           children: [
  //             GetPage(
  //                 name:  _Paths.alertForgotPassword,
  //                 page: () => const AlertForgotPassword(),
  //                 transition: Transition.rightToLeft),
  //           ]
  //       ),
  //     ]),
  // GetPage(
  //     name: _Paths.loginDeepLink,
  //     page: () => const LoginPage(),
  //     binding: AuthBinding(),
  //     middlewares: [
  //       AppMiddleWare(),
  //     ],
  //     children: [
  //       GetPage(
  //           name: _Paths.forgotPassword,
  //           page: () => const ForgotPasswordPage(),
  //           binding: ForgotPasswordBinding(),
  //           transition: Transition.rightToLeft),
  //     ]),
  // GetPage(
  //     name: _Paths.completeRegistration,
  //     page: () => const CompleteRegistrationPage(),
  //     bindings: [CompleteRegistrationBinding(), UserInformationBinding()],
  //     transition: Transition.rightToLeft),
  // GetPage(
  //     name: _Paths.resetPasswordDeepLink,
  //     page: () => const ResetPasswordPage(),
  //     binding: ResetPasswordBinding(),
  //     transition: Transition.rightToLeft),
  // GetPage(
  //     name: _Paths.home,
  //     page: () => const HomePage(),
  //     middlewares: [
  //       AppMiddleWare(),
  //     ],
  //     binding: HomeBinding(),
  //     children: [
  //       GetPage(
  //           name: _Paths.userSetting,
  //           page: () => UserSettingPage(),
  //           transition: Transition.rightToLeft,
  //           children: [
  //             GetPage(
  //                 name: _Paths.userInformation,
  //                 page: () => const UserInformationPage(),
  //                 binding: UserInformationBinding(),
  //                 transition: Transition.rightToLeft),
  //           ]),
  //       GetPage(
  //           name: _Paths.onlinePharmacy,
  //           page: () => OnlinePharmacyPage(),
  //           transition: Transition.rightToLeft,
  //           children: [
  //             GetPage(
  //                 name: _Paths.questionnaire,
  //                 page: () => QuestionnairePage(),
  //                 binding: QuestionnaireBinding(),
  //                 transition: Transition.rightToLeft),
  //             GetPage(
  //                 name: _Paths.pharmacyList,
  //                 page: () => const PharmacyListPage(),
  //                 transition: Transition.rightToLeft,
  //                 binding: PharmacyBinding(),
  //                 children: [
  //                   GetPage(
  //                     name: _Paths.pharmacyInformation,
  //                     page: () => PharmacyInformationPage(),
  //                     transition: Transition.rightToLeft,
  //                   )
  //                 ]),
  //             GetPage(
  //                 name: _Paths.drugList,
  //                 binding: DrugListBinding(),
  //                 page: () => DrugListPage(),
  //                 transition: Transition.rightToLeft,
  //                 children: [
  //                   GetPage(
  //                       name: _Paths.drugCategory,
  //                       page: () => DrugCategoryPage(),
  //                       transition: Transition.rightToLeft),
  //                 ]),
  //             GetPage(
  //                 name: _Paths.drugDetail,
  //                 page: () => const DrugDetailPage(),
  //                 binding: DrugDetailBinding(),
  //                 transition: Transition.rightToLeft),
  //             GetPage(
  //                 name: _Paths.orderHistory,
  //                 page: () => OrderHistoryPage(),
  //                 binding: OrderHistoryBinding(),
  //                 transition: Transition.rightToLeft,
  //                 children: [
  //                   GetPage(
  //                     name: _Paths.orderHistoryDetail,
  //                     page: () => OrderHistoryDetailPage(),
  //                     binding: OrderHistoryDetailBinding(),
  //                     transition: Transition.rightToLeft,
  //                   )
  //                 ]),
  //             GetPage(
  //                 name: _Paths.orderList,
  //                 binding: OrderListBinding(),
  //                 page: () => OrderListPage(),
  //                 transition: Transition.rightToLeft,
  //                 children: [
  //                   GetPage(
  //                       name: _Paths.confirmOrder,
  //                       binding: ConfirmOrderBinding(),
  //                       page: () => const ConfirmOrderPage(),
  //                       transition: Transition.rightToLeft),
  //                 ]),
  //             GetPage(
  //                 name: _Paths.registrationInformation,
  //                 page: () => RegistrationInformationPage(),
  //                 binding: RegistrationInformationBinding(),
  //                 transition: Transition.rightToLeft),
  //           ]),
  //       GetPage(
  //           name: _Paths.consultationService,
  //           page: () => ConsultationServicePage(),
  //           transition: Transition.rightToLeft),
  //     ]),
  // GetPage(
  //     name: _Paths.qa,
  //     page: () => QAPage(),
  //     transition: Transition.rightToLeft),
];
