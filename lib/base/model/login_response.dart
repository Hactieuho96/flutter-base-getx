/// status_code : 200
/// message : null
/// error_code : "NO_ERROR"
/// error_message : null
/// error_data : null

class LoginResponse {
  LoginResponse({
    int? statusCode,
    dynamic message,
    String? errorCode,
    dynamic errorMessage,
    dynamic errorData,
    LoginData? data,
  }) {
    _statusCode = statusCode;
    _message = message;
    _errorCode = errorCode;
    _errorMessage = errorMessage;
    _errorData = errorData;
    _data = data;
  }

  LoginResponse.fromJson(dynamic json) {
    _statusCode = json['status_code'];
    _message = json['message'];
    _errorCode = json['error_code'];
    _errorMessage = json['error_message'];
    _errorData = json['error_data'];
    _data = json['data'] != null ? LoginData.fromJson(json['data']) : null;
  }

  int? _statusCode;
  dynamic _message;
  String? _errorCode;
  dynamic _errorMessage;
  dynamic _errorData;
  LoginData? _data;

  int? get statusCode => _statusCode;

  dynamic get message => _message;

  String? get errorCode => _errorCode;

  dynamic get errorMessage => _errorMessage;

  dynamic get errorData => _errorData;

  LoginData? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status_code'] = _statusCode;
    map['message'] = _message;
    map['error_code'] = _errorCode;
    map['error_message'] = _errorMessage;
    map['error_data'] = _errorData;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// access_token : "555|xwIoDscy0Pcl7Le75WJUdhHbuBHRJQyDtcchylmj"
/// token_type : "Bearer"

class LoginData {
  LoginData({
    String? accessToken,
    String? tokenType,
    User? user,
  }) {
    _accessToken = accessToken;
    _tokenType = tokenType;
    _user = user;
  }

  LoginData.fromJson(dynamic json) {
    _accessToken = json['access_token'];
    _tokenType = json['token_type'];
    _user = json['user'] != null ? User.fromJson(json['user']) : null;
  }

  String? _accessToken;
  String? _tokenType;
  User? _user;

  String? get accessToken => _accessToken;

  String? get tokenType => _tokenType;

  User? get user => _user;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = _accessToken;
    map['token_type'] = _tokenType;
    if (_user != null) {
      map['user'] = _user?.toJson();
    }
    return map;
  }
}

/// id : 12
/// username : "huynhthang"
/// name : "123"
/// email : "huynhthang@gmail.com"
/// phone : null
/// email_verified_at : null
/// active : 1
/// language : "vi"
/// login_at : "2022-06-30T04:18:14.116492Z"
/// click_not_available_counter : 2
/// login_counter : 43
/// roles : {"id":3,"name":"light plan"}
/// profile : {"id":4,"user_id":12,"dob":null,"gender":0,"facebook":null,"line":null,"nationality":null,"visa_type":0,"job_name":0,"education_status":0,"company_name":"test company","person_in_charge":"ttttt","company_email":"huynhthang+1@gmail.com","person_in_charge_contact_information":"test","deleted_at":null,"created_at":"2022-06-28T03:00:51.000000Z","updated_at":"2022-06-28T03:00:51.000000Z"}

class User {
  User({
    int? id,
    String? username,
    String? name,
    String? email,
    dynamic phone,
    dynamic emailVerifiedAt,
    int? active,
    String? avatar,
    String? language,
    String? loginAt,
    int? clickNotAvailableCounter,
    int? loginCounter,
    Roles? roles,
    Profile? profile,
  }) {
    _id = id;
    _username = username;
    _name = name;
    _email = email;
    _phone = phone;
    _emailVerifiedAt = emailVerifiedAt;
    _active = active;
    _avatar = avatar;
    _language = language;
    _loginAt = loginAt;
    _clickNotAvailableCounter = clickNotAvailableCounter;
    _loginCounter = loginCounter;
    _roles = roles;
    _profile = profile;
  }

  User.fromJson(dynamic json) {
    _id = json['id'];
    _username = json['username'];
    _name = json['name'];
    _email = json['email'];
    _phone = json['phone'];
    _emailVerifiedAt = json['email_verified_at'];
    _active = json['active'];
    _avatar = json['avatar'];
    _language = json['language'];
    _loginAt = json['login_at'];
    _clickNotAvailableCounter = json['click_not_available_counter'];
    _loginCounter = json['login_counter'];
    _roles = json['roles'] != null ? Roles.fromJson(json['roles']) : null;
    _profile =
        json['profile'] != null ? Profile.fromJson(json['profile']) : null;
  }

  int? _id;
  String? _username;
  String? _name;
  String? _email;
  dynamic _phone;
  dynamic _emailVerifiedAt;
  int? _active;
  String? _avatar;
  String? _language;
  String? _loginAt;
  int? _clickNotAvailableCounter;
  int? _loginCounter;
  Roles? _roles;
  Profile? _profile;

  int? get id => _id;

  String? get username => _username;

  String? get name => _name;

  String? get email => _email;

  dynamic get phone => _phone;

  dynamic get emailVerifiedAt => _emailVerifiedAt;

  int? get active => _active;

  set active(int? value) {
    active = value;
  }

  String? get avatar => _avatar;

  String? get language => _language;

  String? get loginAt => _loginAt;

  int? get clickNotAvailableCounter => _clickNotAvailableCounter;

  int? get loginCounter => _loginCounter;

  Roles? get roles => _roles;

  Profile? get profile => _profile;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['username'] = _username;
    map['name'] = _name;
    map['email'] = _email;
    map['phone'] = _phone;
    map['email_verified_at'] = _emailVerifiedAt;
    map['active'] = _active;
    map['avatar'] = _avatar;
    map['language'] = _language;
    map['login_at'] = _loginAt;
    map['click_not_available_counter'] = _clickNotAvailableCounter;
    map['login_counter'] = _loginCounter;
    if (_roles != null) {
      map['roles'] = _roles?.toJson();
    }
    if (_profile != null) {
      map['profile'] = _profile?.toJson();
    }
    return map;
  }
}

/// id : 4
/// user_id : 12
/// dob : null
/// gender : 0
/// facebook : null
/// line : null
/// nationality : null
/// visa_type : 0
/// job_name : 0
/// education_status : 0
/// company_name : "test company"
/// person_in_charge : "ttttt"
/// company_email : "huynhthang+1@gmail.com"
/// person_in_charge_contact_information : "test"
/// deleted_at : null
/// created_at : "2022-06-28T03:00:51.000000Z"
/// updated_at : "2022-06-28T03:00:51.000000Z"

class Profile {
  Profile({
    int? id,
    int? userId,
    dynamic dob,
    int? gender,
    dynamic facebook,
    dynamic line,
    dynamic nationality,
    int? visaType,
    int? jobName,
    int? educationStatus,
    String? companyName,
    String? personInCharge,
    String? companyEmail,
    String? personInChargeContactInformation,
    dynamic deletedAt,
    String? createdAt,
    String? updatedAt,
  }) {
    _id = id;
    _userId = userId;
    _dob = dob;
    _gender = gender;
    _facebook = facebook;
    _line = line;
    _nationality = nationality;
    _visaType = visaType;
    _jobName = jobName;
    _educationStatus = educationStatus;
    _companyName = companyName;
    _personInCharge = personInCharge;
    _companyEmail = companyEmail;
    _personInChargeContactInformation = personInChargeContactInformation;
    _deletedAt = deletedAt;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
  }

  Profile.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _dob = json['dob'];
    _gender = json['gender'];
    _facebook = json['facebook'];
    _line = json['line'];
    _nationality = json['nationality'];
    _visaType = json['visa_type'];
    _jobName = json['job_name'];
    _educationStatus = json['education_status'];
    _companyName = json['company_name'];
    _personInCharge = json['person_in_charge'];
    _companyEmail = json['company_email'];
    _personInChargeContactInformation =
        json['person_in_charge_contact_information'];
    _deletedAt = json['deleted_at'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  int? _id;
  int? _userId;
  dynamic _dob;
  int? _gender;
  dynamic _facebook;
  dynamic _line;
  dynamic _nationality;
  int? _visaType;
  int? _jobName;
  int? _educationStatus;
  String? _companyName;
  String? _personInCharge;
  String? _companyEmail;
  String? _personInChargeContactInformation;
  dynamic _deletedAt;
  String? _createdAt;
  String? _updatedAt;

  int? get id => _id;

  int? get userId => _userId;

  dynamic get dob => _dob;

  int? get gender => _gender;

  dynamic get facebook => _facebook;

  dynamic get line => _line;

  dynamic get nationality => _nationality;

  int? get visaType => _visaType;

  int? get jobName => _jobName;

  int? get educationStatus => _educationStatus;

  String? get companyName => _companyName;

  String? get personInCharge => _personInCharge;

  String? get companyEmail => _companyEmail;

  String? get personInChargeContactInformation =>
      _personInChargeContactInformation;

  dynamic get deletedAt => _deletedAt;

  String? get createdAt => _createdAt;

  String? get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['dob'] = _dob;
    map['gender'] = _gender;
    map['facebook'] = _facebook;
    map['line'] = _line;
    map['nationality'] = _nationality;
    map['visa_type'] = _visaType;
    map['job_name'] = _jobName;
    map['education_status'] = _educationStatus;
    map['company_name'] = _companyName;
    map['person_in_charge'] = _personInCharge;
    map['company_email'] = _companyEmail;
    map['person_in_charge_contact_information'] =
        _personInChargeContactInformation;
    map['deleted_at'] = _deletedAt;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }
}

/// id : 3
/// name : "light plan"

class Roles {
  Roles({
    int? id,
    String? name,
  }) {
    _id = id;
    _name = name;
  }

  Roles.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
  }

  int? _id;
  String? _name;

  int? get id => _id;

  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    return map;
  }
}
