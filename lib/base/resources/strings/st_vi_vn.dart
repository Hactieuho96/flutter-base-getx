const Map<String, String> vi = {
  'app_name': 'Plat Network',
  'email': 'Email',
  'password': 'Mật khẩu',
  'remember': 'Ghi nhớ',
  'forgot_password': 'Quên mật khẩu',
  'login': 'Đăng nhập',
  'term_of_use': 'Điều khoản sử dụng',
  'privacy_policy': 'Chính sách bảo mật',
  'languages': 'Ngôn ngữ',
  'invalid_email': 'Vui lòng nhập Địa chỉ email',
  'invalid_password': 'Mật khẩu không hợp lệ',

};
