const Map<String, String> en = {
  'app_name': 'Plat Network',
  'email': 'Email',
  'password': 'Password',
  'remember': 'Remember',
  'forgot_password': 'Forgot password',
  'login': 'Login',
  'term_of_use': 'Term of use',
  'privacy_policy': 'Privacy policy',
  'languages': 'Languages',
  'invalid_email': 'Email address is a required field',
  'invalid_password': 'Invalid password',

};

