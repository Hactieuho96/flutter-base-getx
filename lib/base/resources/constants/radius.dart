import 'package:flutter/material.dart';
import 'dimens.dart';

const Radius radius0 = Radius.circular(dimen0);
const Radius radius1 = Radius.circular(dimen1);
const Radius radius2 = Radius.circular(dimen2);
const Radius radius3 = Radius.circular(dimen3);
const Radius radius4 = Radius.circular(dimen4);
const Radius radius5 = Radius.circular(dimen5);
const Radius radius6 = Radius.circular(dimen6);
const Radius radius7 = Radius.circular(dimen7);
const Radius radius8 = Radius.circular(dimen8);
const Radius radius9 = Radius.circular(dimen9);
const Radius radius10 = Radius.circular(dimen10);
const Radius radius11 = Radius.circular(dimen11);
const Radius radius12 = Radius.circular(dimen12);
const Radius radius13 = Radius.circular(dimen13);
const Radius radius14 = Radius.circular(dimen14);
const Radius radius15 = Radius.circular(dimen15);
const Radius radius16 = Radius.circular(dimen16);
const Radius radius17 = Radius.circular(dimen17);
const Radius radius18 = Radius.circular(dimen18);
const Radius radius19 = Radius.circular(dimen19);
const Radius radius20 = Radius.circular(dimen20);
const Radius radius21 = Radius.circular(dimen21);
const Radius radius22 = Radius.circular(dimen22);
const Radius radius23 = Radius.circular(dimen23);
const Radius radius24 = Radius.circular(dimen24);
const Radius radius25 = Radius.circular(dimen25);
const Radius radius26 = Radius.circular(dimen26);
const Radius radius27 = Radius.circular(dimen27);
const Radius radius28 = Radius.circular(dimen28);
const Radius radius29 = Radius.circular(dimen29);
const Radius radius30 = Radius.circular(dimen30);
const Radius radius31 = Radius.circular(dimen31);
const Radius radius32 = Radius.circular(dimen32);
const Radius radius33 = Radius.circular(dimen33);
const Radius radius34 = Radius.circular(dimen34);
const Radius radius35 = Radius.circular(dimen35);
const Radius radius36 = Radius.circular(dimen36);
const Radius radius37 = Radius.circular(dimen37);
const Radius radius38 = Radius.circular(dimen38);
const Radius radius39 = Radius.circular(dimen39);
const Radius radius40 = Radius.circular(dimen40);
const Radius radius50 = Radius.circular(dimen50);

const BorderRadius border0 = BorderRadius.all(radius0);
const BorderRadius border1 = BorderRadius.all(radius1);
const BorderRadius border2 = BorderRadius.all(radius2);
const BorderRadius border3 = BorderRadius.all(radius3);
const BorderRadius border4 = BorderRadius.all(radius4);
const BorderRadius border5 = BorderRadius.all(radius5);
const BorderRadius border6 = BorderRadius.all(radius6);
const BorderRadius border7 = BorderRadius.all(radius7);
const BorderRadius border8 = BorderRadius.all(radius8);
const BorderRadius border9 = BorderRadius.all(radius9);
const BorderRadius border10 = BorderRadius.all(radius10);
const BorderRadius border11 = BorderRadius.all(radius11);
const BorderRadius border12 = BorderRadius.all(radius12);
const BorderRadius border13 = BorderRadius.all(radius13);
const BorderRadius border14 = BorderRadius.all(radius14);
const BorderRadius border15 = BorderRadius.all(radius15);
const BorderRadius border16 = BorderRadius.all(radius16);
const BorderRadius border17 = BorderRadius.all(radius17);
const BorderRadius border18 = BorderRadius.all(radius18);
const BorderRadius border19 = BorderRadius.all(radius19);
const BorderRadius border20 = BorderRadius.all(radius20);
const BorderRadius border21 = BorderRadius.all(radius21);
const BorderRadius border22 = BorderRadius.all(radius22);
const BorderRadius border23 = BorderRadius.all(radius23);
const BorderRadius border24 = BorderRadius.all(radius24);
const BorderRadius border25 = BorderRadius.all(radius25);
const BorderRadius border26 = BorderRadius.all(radius26);
const BorderRadius border27 = BorderRadius.all(radius27);
const BorderRadius border28 = BorderRadius.all(radius28);
const BorderRadius border29 = BorderRadius.all(radius29);
const BorderRadius border30 = BorderRadius.all(radius30);
const BorderRadius border31 = BorderRadius.all(radius31);
const BorderRadius border32 = BorderRadius.all(radius32);
const BorderRadius border33 = BorderRadius.all(radius33);
const BorderRadius border34 = BorderRadius.all(radius34);
const BorderRadius border35 = BorderRadius.all(radius35);
const BorderRadius border36 = BorderRadius.all(radius36);
const BorderRadius border37 = BorderRadius.all(radius37);
const BorderRadius border38 = BorderRadius.all(radius38);
const BorderRadius border39 = BorderRadius.all(radius39);
const BorderRadius border40 = BorderRadius.all(radius40);
const BorderRadius border50 = BorderRadius.all(radius50);
