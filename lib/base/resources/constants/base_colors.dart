import 'package:flutter/material.dart';

const Color colorBackground = colorFAF9F9;
const Color colorPrimary = color177FE2;
const Color colorSecondary = colorFFB800;
const Color colorTextPrimary = color171716;
const Color colorDisabledButton = color32171716;
const Color colorDialogBackground = Color(0x2A4E4E4E);

const Color colorError = colorD13E45;
const Color colorWhite = Colors.white;
const Color colorBlack = Colors.black;
const Color colorBlack50 = Color(0x80000000);
const Color colorTextMain = color32302D;
const Color colorTextHint = color9C9896;
const Color colorTransparent = Colors.transparent;

const Color color177FE2 = Color(0xff177FE2);
const Color color32171716 = Color(0x52171716);
const Color colorFAF9F9 = Color(0xffFAF9F9);
const Color color171716 = Color(0xff171716);
const Color colorF3F1F1 = Color(0xffF3F1F1);
const Color color32302D = Color(0xff32302D);
const Color color9C9896 = Color(0xff9C9896);
const Color colorD13E45 = Color(0xffD13E45);
const Color colorFFB800 = Color(0xffFFB800);
const Color color4E4E4E = Color(0xff4E4E4E);
const Color colorF5F7F9 = Color(0xffF5F7F9);
const Color color62A19B = Color(0xff62A19B);
const Color color1D71F2 = Color(0xff1D71F2);
const Color color1DA5F2 = Color(0xff1DA5F2);
const Color color3F51B5 = Color(0xff3F51B5);
const Color color898989 = Color(0xff898989);
const Color color2C2C2C = Color(0xff2C2C2C);
const Color color27AE60 = Color(0xff27AE60);
const Color colorFF4242 = Color(0xffFF4242);
const Color color4E5260 = Color(0xff4E5260);
const Color colorDCDCDC = Color(0xffDCDCDC);
const Color color495057 = Color(0xff495057);
const Color colorE5E5E5 = Color(0xffE5E5E5);
const Color colorCCCCCC = Color(0xffCCCCCC);
const Color colorB7BBCB = Color(0xffB7BBCB);
const Color color9C9C9C = Color(0xff9C9C9C);

Color hexToColor(String code) {
  return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}
