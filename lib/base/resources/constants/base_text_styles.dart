import 'package:flutter/material.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/resources/constants/dimens.dart';

const text10_white_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen10,
    fontWeight: FontWeight.w600,
    color: colorWhite);

const text12_primary_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w400,
    color: colorTextPrimary);
const text12_primary_700 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w700,
    color: colorTextPrimary);
const text12_error_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w400,
    color: colorError);
const text12_error_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w600,
    color: colorError);
const text12_898989_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w400,
    color: color898989);
const text12_2C2C2C_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w400,
    color: color2C2C2C);
const text12_2C2C2C_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w600,
    color: color2C2C2C);
const text12_4E4E4E_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen12,
    fontWeight: FontWeight.w400,
    color: color4E4E4E);

const text14_primary_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: colorTextPrimary);
const text14_primary_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w600,
    color: colorPrimary);
const text14_white_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: colorWhite);
const text14_white_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w600,
    color: colorWhite);
const text14_9C9896_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: color9C9896);
const text14_898989_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: color898989);
const text14_898989_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w600,
    color: color898989);
const text14_2C2C2C_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: color2C2C2C);
const text14_2C2C2C_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w600,
    color: color2C2C2C);
const text14_error_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w600,
    color: colorError);
const text14_4E4E4E_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen14,
    fontWeight: FontWeight.w400,
    color: color4E4E4E);

const text15_white_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen15,
    fontWeight: FontWeight.w600,
    color: colorWhite);

const text16_32302D_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen16,
    fontWeight: FontWeight.w400,
    color: color32302D);
const text16_898989_400 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen16,
    fontWeight: FontWeight.w400,
    color: color898989);
const text16_2C2C2C_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen16,
    fontWeight: FontWeight.w600,
    color: color2C2C2C,
    height: 1.6);
const text16_32302D_600 = TextStyle(
  fontFamily: 'AvenirNext',
  fontSize: dimen16,
  fontWeight: FontWeight.w600,
  color: color32302D
);
const text16_error_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen16,
    fontWeight: FontWeight.w600,
    color: colorError);
const text16_primary_700 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen16,
    fontWeight: FontWeight.w700,
    color: colorTextPrimary);

const text18_white_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen18,
    fontWeight: FontWeight.w600,
    color: colorWhite);
const text18_898989_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen18,
    fontWeight: FontWeight.w600,
    color: color898989);
const text18_primary_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen18,
    fontWeight: FontWeight.w600,
    color: colorTextPrimary);
const text18_2C2C2C_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen18,
    fontWeight: FontWeight.w600,
    color: color2C2C2C,
    height: 1.6);

const text20_white_600 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen20,
    fontWeight: FontWeight.w600,
    color: colorWhite);
const text20_error_700 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen20,
    fontWeight: FontWeight.w700,
    color: colorError);

const text24_primary_700 = TextStyle(
    fontFamily: 'AvenirNext',
    fontSize: dimen24,
    fontWeight: FontWeight.w700,
    color: colorTextPrimary);
