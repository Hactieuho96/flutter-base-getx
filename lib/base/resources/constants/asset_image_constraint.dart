part of 'base_constraint.dart';

class AssetImagePath {
  static const icon_snack_bar_success = 'icon_snack_bar_success';
  static const icon_snack_bar_error = 'icon_snack_bar_error';
  static const clear = 'clear';
  static const image_error = 'image_error';
  static const no_data = 'no_data';
  static const eye = 'eye';
  static const eye_disabled = 'eye_disabled';

}