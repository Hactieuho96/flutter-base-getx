import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:plat_app/base/controllers/auth/auth_controller.dart';
import 'package:plat_app/base/controllers/language/language_controller.dart';
import 'package:plat_app/base/controllers/language/localization_service.dart';
import 'package:plat_app/base/resources/constants/base_colors.dart';
import 'package:plat_app/base/routes/base_pages.dart';

Future main() async {
  // Load env
  await dotenv.load(fileName: '.env');
  final variant = dotenv.env['VARIANT'];
  await dotenv.load(fileName: '.env.$variant');

  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialBinding: BindingsBuilder(() {
        Get.put(LanguageController(), permanent: true);
        Get.put(AuthController());
      }),
      locale: LocalizationService.locale,
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
      initialRoute: Routes.splash,
      getPages: pages,
      theme: ThemeData(
          fontFamily: 'AvenirNext',
          scaffoldBackgroundColor: colorBackground,
          primaryColor: colorPrimary,
          toggleableActiveColor: colorPrimary,),
    );
  }
}
